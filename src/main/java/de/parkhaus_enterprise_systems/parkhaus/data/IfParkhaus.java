/**
 * (c) 2017 Parkhaus Enterprise Systems
 */
package de.parkhaus_enterprise_systems.parkhaus.data;

import java.util.List;

import de.parkhaus_enterprise_systems.parkhaus.data.accounting.IfControllingUnit;
import de.parkhaus_enterprise_systems.parkhaus.data.mangement.IfManagementUnit;
import de.parkhaus_enterprise_systems.parkhaus.util.EnParkplatz;

/**
 * @author Kevin
 *
 */
public interface IfParkhaus
{

	int getAnzahlParkplaetze(EnParkplatz typ);

	int getFreieParkplaetze(EnParkplatz typ);

	int getBelegteParkplaetze(EnParkplatz typ);

	boolean hatFreienParkplatz(EnParkplatz typ);

	boolean hasKennzeichen(String kennzeichen);

	boolean fahrzeugEinfahren(IfKfz fahrzeug);

	boolean fahrzeugAusfahren(IfKfz fahrzeug);
	
	long getOeffnungszeit();
	
	long getSchliessungszeit();

	IfKfz getFirstKfz();

	List<IfEtage> getEtagen();

	IfControllingUnit getControllingUnit();

	IfManagementUnit getManagementUnit();
}
