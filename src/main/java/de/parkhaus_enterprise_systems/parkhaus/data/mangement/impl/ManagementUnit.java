/**
 * (c) 2017 Parkhaus Enterprise Systems
 */
package de.parkhaus_enterprise_systems.parkhaus.data.mangement.impl;

import de.parkhaus_enterprise_systems.parkhaus.data.mangement.IfManagementUnit;

/**
 * @author Kevin
 *
 */
public class ManagementUnit implements IfManagementUnit
{
	double preisProStunde = 0.;
	double ausgabenProStunde = 0;
	
	long oeffnungszeit, schliessungszeit;

	public ManagementUnit()
	{
		this(1);
	}

	public ManagementUnit(double preisProStunde)
	{
		this.preisProStunde = preisProStunde;
	}

	@Override
	public double getPreisProStunde()
	{
		return preisProStunde;
	}
	
	@Override
	public double getAusgabenProStunde() {
		return ausgabenProStunde;
	}

	@Override
	public void setPreisProStunde(double preis)
	{
		this.preisProStunde = preis;
	}

	@Override
	public void setAusgabenProStunde(double ausgaben)
	{
		this.ausgabenProStunde = ausgaben;
	}

	@Override
	public long getOeffnungszeit()
	{
		// TODO Auto-generated method stub
		return oeffnungszeit;
	}

	@Override
	public long getSchliessungszeit()
	{
		// TODO Auto-generated method stub
		return schliessungszeit;
	}

	@Override
	public void setOeffnungszeit(long o)
	{
		// TODO Auto-generated method stub
		this.oeffnungszeit = o;
	}

	@Override
	public void setSchliessungszeit(long s)
	{
		// TODO Auto-generated method stub
		this.schliessungszeit = s;
	}

}
