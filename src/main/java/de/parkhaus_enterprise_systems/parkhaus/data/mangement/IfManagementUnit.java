/**
 * (c) 2017 Parkhaus Enterprise Systems
 */
package de.parkhaus_enterprise_systems.parkhaus.data.mangement;

/**
 * @author Kevin
 *
 */
public interface IfManagementUnit
{

	double getPreisProStunde();
	void setPreisProStunde(double preis);
	double getAusgabenProStunde();
	void setAusgabenProStunde(double ausgaben);
	long getOeffnungszeit();
	long getSchliessungszeit();
	void setOeffnungszeit(long o);
	void setSchliessungszeit(long s);
}
