/**
 * (c) 2017 Parkhaus Enterprise Systems
 */
package de.parkhaus_enterprise_systems.parkhaus.data;

/**
 * @author Kevin
 *
 */
public interface IfSerialize
{

	String saveObject();

	void loadObject(String xml);
}
