/**
 * (c) 2017 Parkhaus Enterprise Systems
 */
package de.parkhaus_enterprise_systems.parkhaus.data;

import java.util.List;

import de.parkhaus_enterprise_systems.parkhaus.util.EnParkplatz;

/**
 * @author Kevin
 *
 */
public interface IfEtage
{
	int getAnzahlParkplaetze(EnParkplatz typ);

	List<IfParkplatz> getAlleParkplaetze();

	List<IfParkplatz> getBelegteParkplaetze();

	int getFreieParkplaetze(EnParkplatz typ);

	int getBelegteParkplaetze(EnParkplatz typ);

	boolean hatFreienParkplatz(EnParkplatz typ);

	IfParkplatz getFirstFreienParkplatz(EnParkplatz typ);

	boolean hasKennzeichen(String kennzeichen);

	int fahrzeugAusfahren(IfKfz fahrzeug);

	IfKfz getFirstKfz();
}
