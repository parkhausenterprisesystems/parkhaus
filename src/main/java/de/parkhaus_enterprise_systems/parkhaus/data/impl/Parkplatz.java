/**
 * (c) 2017 Parkhaus Enterprise Systems
 */
package de.parkhaus_enterprise_systems.parkhaus.data.impl;

import de.parkhaus_enterprise_systems.parkhaus.data.IfKfz;
import de.parkhaus_enterprise_systems.parkhaus.data.IfParkplatz;
import de.parkhaus_enterprise_systems.parkhaus.util.EnParkplatz;

/**
 * @author Sascha
 *
 */
public class Parkplatz implements IfParkplatz
{

	private EnParkplatz typ;
	private IfKfz fahrzeug = null;

	public Parkplatz()
	{
		this(EnParkplatz.NORMAL);
	}

	public Parkplatz(EnParkplatz typ)
	{
		this.typ = typ;
	}

	@Override
	public boolean istBelegt()
	{
		return fahrzeug != null;
	}

	@Override
	public EnParkplatz getTyp()
	{
		return typ;
	}

	@Override
	public IfKfz getGeparktesFahrzeug()
	{
		return fahrzeug;
	}

	@Override
	public boolean parkeFahrzeug(IfKfz kfz)
	{
		if (istBelegt())
		{
			return false;
		}
		fahrzeug = kfz;
		return true;
	}

	@Override
	public boolean fahrzeugAusfahren(IfKfz kfz)
	{
		kfz.ausfahrt();
		this.fahrzeug = null;
		return true;
	}

	@Override
	public boolean hasFahrzeug()
	{
		return this.fahrzeug != null;
	}

}
