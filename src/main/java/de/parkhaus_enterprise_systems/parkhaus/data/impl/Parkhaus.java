/**
 * (c) 2017 Parkhaus Enterprise Systems
 */
package de.parkhaus_enterprise_systems.parkhaus.data.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import de.parkhaus_enterprise_systems.parkhaus.data.IfEtage;
import de.parkhaus_enterprise_systems.parkhaus.data.IfKfz;
import de.parkhaus_enterprise_systems.parkhaus.data.IfParkhaus;
import de.parkhaus_enterprise_systems.parkhaus.data.IfParkplatz;
import de.parkhaus_enterprise_systems.parkhaus.data.accounting.IfControllingUnit;
import de.parkhaus_enterprise_systems.parkhaus.data.accounting.IfStatistik;
import de.parkhaus_enterprise_systems.parkhaus.data.accounting.impl.ControllingUnit;
import de.parkhaus_enterprise_systems.parkhaus.data.accounting.impl.Statistik;
import de.parkhaus_enterprise_systems.parkhaus.data.mangement.IfManagementUnit;
import de.parkhaus_enterprise_systems.parkhaus.data.mangement.impl.ManagementUnit;
import de.parkhaus_enterprise_systems.parkhaus.util.EnGeschlecht;
import de.parkhaus_enterprise_systems.parkhaus.util.EnParkplatz;

/**
 * @author Sascha
 *
 */
public class Parkhaus implements IfParkhaus
{
	private final List<IfEtage> etagen;
	protected List<String> kennzeichenliste;
	private IfControllingUnit controllingUnit = null;
	private IfManagementUnit managementUnit = null;
	private long oeffnungszeit;
	private long schliessungszeit;

	public Parkhaus()
	{
		this(0, 0);
	}

	public Parkhaus(int etagen, int parkplaetzeProEtage)
	{
		this(etagen, parkplaetzeProEtage, new ManagementUnit(1.2));
	}

	public Parkhaus(int etagen, int parkplaetzeProEtage, IfManagementUnit managementUnit)
	{
		this(etagen, parkplaetzeProEtage, 1, 0, managementUnit);
	}

	public Parkhaus(int etagen, int parkplaetzeProEtage, int disabled, int women, IfManagementUnit managementUnit)
	{
		this.etagen = new ArrayList<IfEtage>();
		this.kennzeichenliste = new ArrayList<>();
		for (int i = 1; i <= etagen; i++)
		{
			IfEtage etage = new Etage(parkplaetzeProEtage, disabled, women);
			this.etagen.add(etage);
		}
		this.controllingUnit = new ControllingUnit(this);
		this.managementUnit = managementUnit;
	}

	@Override
	public int getAnzahlParkplaetze(EnParkplatz typ)
	{
		if (typ == null)
		{
			return -1;
		}
		int anzahl = 0;
		for (IfEtage etage : this.etagen)
		{
			anzahl += etage.getAnzahlParkplaetze(typ);
		}
		return anzahl;
	}

	@Override
	public int getFreieParkplaetze(EnParkplatz typ)
	{
		if (typ == null)
		{
			return 0;
		}
		return this.etagen.stream().mapToInt(e -> e.getFreieParkplaetze(typ)).sum();
	}

	@Override
	public boolean hatFreienParkplatz(EnParkplatz typ)
	{
		return getFreieParkplaetze(typ) > 0;
	}

	@Override
	public boolean hasKennzeichen(String kennzeichen)
	{
		for (String k : this.kennzeichenliste)
		{
			if (k.equals(kennzeichen))
			{
				return true;
			}
		}
		return false;
	}

	@Override
	public boolean fahrzeugEinfahren(IfKfz fahrzeug)
	{
		if (fahrzeug == null || hasKennzeichen(fahrzeug.getKennzeichen()))
		{
			return false;
		}
		fahrzeug.einfahrt();
		kennzeichenliste.add(fahrzeug.getKennzeichen());
		EnParkplatz typ = EnParkplatz.NORMAL;
		if (fahrzeug.getGeschlecht().equals(EnGeschlecht.WEIBLICH))
		{
			typ = EnParkplatz.FRAUEN;
		}
		if (fahrzeug.hatBehinderung())
		{
			typ = EnParkplatz.BEHINDERT;
		}
		boolean parked = false;
		for (IfEtage e : getEtagen())
		{
			if (e.hatFreienParkplatz(typ))
			{
				IfParkplatz firstFreienParkplatz = e.getFirstFreienParkplatz(typ);
				parked = firstFreienParkplatz.parkeFahrzeug(fahrzeug);
				if (parked)
				{
					break;
				}
			}
		}
		if (!parked)
		{
			for (IfEtage e : getEtagen())
			{
				if (e.hatFreienParkplatz(EnParkplatz.NORMAL))
				{
					IfParkplatz firstFreienParkplatz = e.getFirstFreienParkplatz(EnParkplatz.NORMAL);
					parked = firstFreienParkplatz.parkeFahrzeug(fahrzeug);
					if (parked)
					{
						break;
					}
				}
			}
		}
		return parked;
	}

	@Override
	public List<IfEtage> getEtagen()
	{
		return this.etagen;
	}

	@Override
	public IfControllingUnit getControllingUnit()
	{
		return controllingUnit;
	}

	@Override
	public IfManagementUnit getManagementUnit()
	{
		return managementUnit;
	}

	@Override
	public boolean fahrzeugAusfahren(IfKfz fahrzeug)
	{
		for (int i = 0; i < this.etagen.size(); i++)
		{
			IfEtage e = this.etagen.get(i);
			int pIndex = e.fahrzeugAusfahren(fahrzeug);
			if (pIndex >= 0)
			{
				IfStatistik k = new Statistik(i, pIndex, fahrzeug, this.managementUnit);
				this.controllingUnit.addStatistik(k);
				this.kennzeichenliste.remove(fahrzeug.getKennzeichen());
				return true;
			}
		}
		return false;
	}

	@Override
	public IfKfz getFirstKfz()
	{
		Optional<IfEtage> findFirst = this.etagen.stream().filter(e -> e.getFirstKfz() != null).findFirst();
		if (findFirst.isPresent())
		{
			return findFirst.get().getFirstKfz();
		}
		return null;
	}

	@Override
	public int getBelegteParkplaetze(EnParkplatz typ)
	{
		if (typ == null)
		{
			return 0;
		}
		return this.etagen.stream().mapToInt(e -> e.getBelegteParkplaetze(typ)).sum();
	}

	@Override
	public long getOeffnungszeit()
	{
		// TODO Auto-generated method stub
		return oeffnungszeit;
	}

	@Override
	public long getSchliessungszeit()
	{
		// TODO Auto-generated method stub
		return schliessungszeit;
	}

}
