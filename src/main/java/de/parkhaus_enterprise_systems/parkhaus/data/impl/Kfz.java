/**
 * (c) 2017 Parkhaus Enterprise Systems
 */
package de.parkhaus_enterprise_systems.parkhaus.data.impl;

import com.thoughtworks.xstream.XStream;

import de.parkhaus_enterprise_systems.parkhaus.data.IfKfz;
import de.parkhaus_enterprise_systems.parkhaus.util.EnGeschlecht;
import de.parkhaus_enterprise_systems.parkhaus.util.TimeUtil;

/**
 * @author Kevin
 *
 */
public class Kfz implements IfKfz
{
	private Long ankunft = null;
	private Long abfahrt = null;
	private String kennzeichen;
	private EnGeschlecht geschlecht;
	private String farbe;
	private boolean behinderung;

	public Kfz(String kennzeichen, EnGeschlecht geschlecht, String farbe, boolean behinderung)
	{
		this.kennzeichen = kennzeichen;
		this.geschlecht = geschlecht;
		this.farbe = farbe;
		this.behinderung = behinderung;
	}

	public Kfz(String kennzeichen)
	{
		this(kennzeichen, EnGeschlecht.MAENNLICH, "Rot", false);
	}

	@Override
	public String getKennzeichen()
	{
		return this.kennzeichen;
	}

	@Override
	public EnGeschlecht getGeschlecht()
	{
		return this.geschlecht;
	}

	@Override
	public String getFarbe()
	{
		return this.farbe;
	}

	@Override
	public boolean hatBehinderung()
	{
		return this.behinderung;
	}

	@Override
	public Long getEinfahrtszeit()
	{
		return this.ankunft;
	}

	@Override
	public Long getAusfahrtszeit()
	{
		return this.abfahrt;
	}

	@Override
	public void einfahrt()
	{
		this.ankunft = System.currentTimeMillis();
	}

	@Override
	public Long ausfahrt()
	{
		this.abfahrt = System.currentTimeMillis();
		return TimeUtil.berechneParkzeit(ankunft, abfahrt);
	}

	@Override
	public String saveObject()
	{

		return (new XStream()).toXML(this);
	}

	@Override
	public void loadObject(String xml)
	{
		Kfz kfz = (Kfz) (new XStream()).fromXML(xml);
		this.abfahrt = kfz.abfahrt;
		this.ankunft = kfz.ankunft;
		this.behinderung = kfz.behinderung;
		this.geschlecht = kfz.geschlecht;
		this.farbe = kfz.farbe;
		this.kennzeichen = kfz.kennzeichen;
	}

	public boolean equals(IfKfz obj)
	{
		return this.kennzeichen.equals(obj.getKennzeichen()) && this.geschlecht.equals(obj.getGeschlecht());
	}
}
