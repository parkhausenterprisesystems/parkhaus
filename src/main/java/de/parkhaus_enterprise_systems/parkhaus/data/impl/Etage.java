/**
 * (c) 2017 Parkhaus Enterprise Systems
 */
package de.parkhaus_enterprise_systems.parkhaus.data.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import de.parkhaus_enterprise_systems.parkhaus.data.IfEtage;
import de.parkhaus_enterprise_systems.parkhaus.data.IfKfz;
import de.parkhaus_enterprise_systems.parkhaus.data.IfParkplatz;
import de.parkhaus_enterprise_systems.parkhaus.util.EnParkplatz;

/**
 * @author Sascha
 *
 */
public class Etage implements IfEtage
{
	private final List<IfParkplatz> parkplatz;

	public Etage()
	{
		this(0);
	}

	public Etage(int parkplaetze)
	{
		this(parkplaetze, 0, 0);
	}

	public Etage(int normal, int frauen, int behindert)
	{
		parkplatz = new ArrayList<IfParkplatz>();
		for (int n = 0; n < normal; n++)
		{
			IfParkplatz p = new Parkplatz();
			parkplatz.add(p);
		}
		for (int n = 0; n < frauen; n++)
		{
			IfParkplatz p = new Parkplatz(EnParkplatz.FRAUEN);
			parkplatz.add(p);
		}
		for (int n = 0; n < behindert; n++)
		{
			IfParkplatz p = new Parkplatz(EnParkplatz.BEHINDERT);
			parkplatz.add(p);
		}
	}

	@Override
	public int getAnzahlParkplaetze(EnParkplatz typ)
	{
		return getParkplaetze(typ).size();
	}

	private List<IfParkplatz> getParkplaetze(EnParkplatz typ)
	{
		return this.parkplatz.stream().filter(p -> p.getTyp().equals(typ)).collect(Collectors.toList());
	}

	@Override
	public List<IfParkplatz> getAlleParkplaetze()
	{
		return parkplatz;
	}

	@Override
	public int getFreieParkplaetze(EnParkplatz typ)
	{
		return this.parkplatz.stream().filter(p -> p.getTyp().equals(typ) && !p.istBelegt())
				.collect(Collectors.toList()).size();
	}

	@Override
	public boolean hatFreienParkplatz(EnParkplatz typ)
	{
		return this.parkplatz.stream().filter(p -> p.getTyp().equals(typ) && !p.istBelegt()).findFirst().isPresent();
	}

	@Override
	public boolean hasKennzeichen(String kennzeichen)
	{
		return this.parkplatz.stream()
				.filter(p -> p.hasFahrzeug() && p.getGeparktesFahrzeug().getKennzeichen().equals(kennzeichen))
				.findFirst().isPresent();
	}

	@Override
	public List<IfParkplatz> getBelegteParkplaetze()
	{
		return this.parkplatz.stream().filter(p -> !p.istBelegt()).collect(Collectors.toList());
	}

	@Override
	public int fahrzeugAusfahren(IfKfz fahrzeug)
	{
		for (int i = 0; i < this.parkplatz.size(); i++)
		{
			IfParkplatz p = this.parkplatz.get(i);
			if (p.hasFahrzeug() && p.getGeparktesFahrzeug().equals(fahrzeug))
			{
				p.fahrzeugAusfahren(fahrzeug);
				return i;
			}
		}
		return -1;
	}

	@Override
	public int getBelegteParkplaetze(EnParkplatz typ)
	{
		return this.parkplatz.stream().filter(p -> p.getTyp().equals(typ) && p.istBelegt()).collect(Collectors.toList())
				.size();
	}

	@Override
	public IfParkplatz getFirstFreienParkplatz(EnParkplatz typ)
	{
		Optional<IfParkplatz> findFirst = this.parkplatz.stream().filter(p -> p.getTyp().equals(typ) && !p.istBelegt())
				.findFirst();
		if (findFirst.isPresent())
		{
			return findFirst.get();
		}
		return null;
	}

	@Override
	public IfKfz getFirstKfz()
	{
		Optional<IfParkplatz> findFirst = this.parkplatz.stream().filter(p -> p.hasFahrzeug()).findFirst();
		if (findFirst.isPresent())
		{
			return findFirst.get().getGeparktesFahrzeug();
		}
		return null;
	}

}
