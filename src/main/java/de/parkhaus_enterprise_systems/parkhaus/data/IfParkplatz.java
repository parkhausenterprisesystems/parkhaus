/**
 * (c) 2017 Parkhaus Enterprise Systems
 */
package de.parkhaus_enterprise_systems.parkhaus.data;

import de.parkhaus_enterprise_systems.parkhaus.util.EnParkplatz;

/**
 * @author Kevin
 *
 */
public interface IfParkplatz
{
	boolean istBelegt();

	EnParkplatz getTyp();

	IfKfz getGeparktesFahrzeug();

	boolean parkeFahrzeug(IfKfz kfz);

	boolean fahrzeugAusfahren(IfKfz kfz);

	boolean hasFahrzeug();
}
