/**
 * (c) 2017 Parkhaus Enterprise Systems
 */
package de.parkhaus_enterprise_systems.parkhaus.data;

import de.parkhaus_enterprise_systems.parkhaus.util.EnGeschlecht;

/**
 * @author Kevin
 *
 */
public interface IfKfz extends IfSerialize
{
	String getKennzeichen();

	EnGeschlecht getGeschlecht();

	String getFarbe();

	boolean hatBehinderung();

	Long getEinfahrtszeit();

	Long getAusfahrtszeit();

	void einfahrt();

	Long ausfahrt();
}
