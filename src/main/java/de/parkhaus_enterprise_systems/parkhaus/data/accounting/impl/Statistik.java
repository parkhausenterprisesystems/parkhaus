/**
 * (c) 2017 Parkhaus Enterprise Systems
 */
package de.parkhaus_enterprise_systems.parkhaus.data.accounting.impl;

import java.util.Calendar;

import de.parkhaus_enterprise_systems.parkhaus.data.IfKfz;
import de.parkhaus_enterprise_systems.parkhaus.data.accounting.IfStatistik;
import de.parkhaus_enterprise_systems.parkhaus.data.accounting.IfZeitpunkt;
import de.parkhaus_enterprise_systems.parkhaus.data.mangement.IfManagementUnit;
import de.parkhaus_enterprise_systems.parkhaus.util.EnGeschlecht;
import de.parkhaus_enterprise_systems.parkhaus.util.TimeUtil;

/**
 * @author Kevin
 *
 */
public class Statistik implements IfStatistik
{

	protected int etagenIndex, parkplatzIndex;
	protected IfKfz kfz;
	protected IfManagementUnit manage;
	protected IfZeitpunkt zeitpunt;
	protected int tag = 1;

	public Statistik(int etagenIndex, int parkplatzIndex, IfKfz kfz, IfManagementUnit manage)
	{
		this.etagenIndex = etagenIndex;
		this.parkplatzIndex = parkplatzIndex;
		this.kfz = kfz;
		this.manage = manage;
		this.zeitpunt = new Zeitpunkt();
		this.tag = Calendar.getInstance().get(Calendar.DATE);
	}

	@Override
	public int getEtageIndex()
	{
		return this.etagenIndex;
	}

	@Override
	public int getParkplatzIndex()
	{
		return this.parkplatzIndex;
	}

	@Override
	public String getKennzeichen()
	{
		return kfz.getKennzeichen();
	}

	@Override
	public IfZeitpunkt getZeitpunkt()
	{
		return zeitpunt;
	}

	@Override
	public Long getEinfahrtszeit()
	{
		return kfz.getEinfahrtszeit();
	}

	@Override
	public Long getAusfahrtszeit()
	{
		return kfz.getAusfahrtszeit();
	}

	@Override
	public double getBezahlterPreis()
	{
		int stunden = TimeUtil.getGeparkteStunden(kfz.getEinfahrtszeit(), kfz.getAusfahrtszeit());
		int minuten = TimeUtil.getGeparkteMinuten(kfz.getEinfahrtszeit(), kfz.getAusfahrtszeit());
		return stunden * manage.getPreisProStunde() + (minuten > 0 ? manage.getPreisProStunde() : 0);
	}

	@Override
	public String getFahrzeugFarbe()
	{
		return kfz.getFarbe();
	}

	@Override
	public boolean hasFahrzeugbehinderung()
	{
		return kfz.hatBehinderung();
	}

	@Override
	public EnGeschlecht getFahrzeugGeschlecht()
	{
		return kfz.getGeschlecht();
	}

	@Override
	public int getTag()
	{
		return tag;
	}

	@Override
	public Long getOeffnungszeit()
	{
		// TODO Auto-generated method stub
		return manage.getOeffnungszeit();
	}

	@Override
	public Long getSchliessungszeit()
	{
		// TODO Auto-generated method stub
		return manage.getSchliessungszeit();
	}

}
