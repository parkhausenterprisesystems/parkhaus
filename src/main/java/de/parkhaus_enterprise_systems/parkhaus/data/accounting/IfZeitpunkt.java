/**
 * (c) 2017 Parkhaus Enterprise Systems
 */
package de.parkhaus_enterprise_systems.parkhaus.data.accounting;

/**
 * @author Kevin
 *
 */
public interface IfZeitpunkt
{
	int getMonat();

	int getJahr();

	boolean equals(IfZeitpunkt obj);
}
