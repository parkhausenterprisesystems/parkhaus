/**
 * (c) 2018 Parkhaus Enterprise Systems
 */
package de.parkhaus_enterprise_systems.parkhaus.data.accounting.impl;

import de.parkhaus_enterprise_systems.parkhaus.data.accounting.IfAuswertung;
import de.parkhaus_enterprise_systems.parkhaus.data.accounting.IfZeitpunkt;

/**
 * @author Kevin
 *
 */
public class Auswertung implements IfAuswertung
{
	private IfZeitpunkt zeitpunkt;
	private double einnahmen;
	private double ausgaben;

	public Auswertung(IfZeitpunkt zeitpunkt, double einnahmen, double ausgaben)
	{
		this.zeitpunkt = zeitpunkt;
		this.einnahmen = einnahmen;
		this.ausgaben = ausgaben;
	}

	@Override
	public IfZeitpunkt getZeitpunkt()
	{
		return zeitpunkt;
	}

	@Override
	public double getEinnahmen()
	{
		return einnahmen;
	}

	@Override
	public double getAusgaben()
	{
		return ausgaben;
	}

	@Override
	public boolean hasGewinn()
	{
		return einnahmen - ausgaben > 0;
	}

	@Override
	public boolean hasVerlust()
	{
		return einnahmen - ausgaben < 0;
	}

}
