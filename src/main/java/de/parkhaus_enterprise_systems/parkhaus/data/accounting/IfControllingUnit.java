/**
 * (c) 2017 Parkhaus Enterprise Systems
 */
package de.parkhaus_enterprise_systems.parkhaus.data.accounting;

import java.util.List;

import de.parkhaus_enterprise_systems.parkhaus.data.accounting.impl.einnahmen.AbstractEinnahmenCalculator;

/**
 * @author Kevin
 *
 */
public interface IfControllingUnit
{
	void addStatistik(IfStatistik stat);

	double getEinnahmen(IfZeitpunkt zeitpunkt);

	double getAusgaben(IfZeitpunkt zeitpunkt);

	double getEinnhamen();

	double getAusgaben();

	double getGewinn(IfZeitpunkt zeitpunkt);

	double getGewinn();

	boolean hasGewinn();

	boolean hasVerlust();

	boolean hasGewinn(IfZeitpunkt zeitpunkt);

	boolean hasVerlust(IfZeitpunkt zeitpunkt);

	List<IfAuswertung> getAuswertung(IfZeitpunkt von, IfZeitpunkt bis);

	int getAnzahlFahrzeuge(IfZeitpunkt zeitpunkt);

	int getAnzahlFahrzeuge();

	double getDurchschnittlicheParkzeit();

	double getDurchschnittlicheParkzeit(IfZeitpunkt zeitpunkt);

	List<IfStatistik> getStatstikInZeitraum(IfZeitpunkt von, IfZeitpunkt bis);

	List<IfStatistik> getStatistik();

	List<IfStatistik> getStatstikForZeitpunkt(IfZeitpunkt z);

	double berechneEinnahmen(IfZeitpunkt zeitpunkt, int tag, AbstractEinnahmenCalculator calculator);
}
