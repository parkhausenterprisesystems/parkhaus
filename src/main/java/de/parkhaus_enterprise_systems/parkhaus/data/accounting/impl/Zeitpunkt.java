/**
 * (c) 2018 Parkhaus Enterprise Systems
 */
package de.parkhaus_enterprise_systems.parkhaus.data.accounting.impl;

import java.util.Calendar;

import de.parkhaus_enterprise_systems.parkhaus.data.accounting.IfZeitpunkt;

/**
 * @author Kevin
 *
 */
public class Zeitpunkt implements IfZeitpunkt
{

	private int monat;
	private int jahr;

	public Zeitpunkt()
	{
		this(Calendar.getInstance().get(Calendar.MONTH) + 1, Calendar.getInstance().get(Calendar.YEAR));
	}

	public Zeitpunkt(int monat, int jahr)
	{
		this.monat = monat;
		this.jahr = jahr;
	}

	@Override
	public int getMonat()
	{
		return this.monat;
	}

	@Override
	public int getJahr()
	{
		return this.jahr;
	}

	public boolean equals(IfZeitpunkt obj)
	{
		return this.getMonat() == obj.getMonat() && this.getJahr() == obj.getJahr();
	}

}
