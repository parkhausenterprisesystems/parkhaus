/**
 * (c) 2018 Parkhaus Enterprise Systems
 */
package de.parkhaus_enterprise_systems.parkhaus.data.accounting.impl.einnahmen;

import java.util.List;

import de.parkhaus_enterprise_systems.parkhaus.data.accounting.IfStatistik;
import de.parkhaus_enterprise_systems.parkhaus.data.accounting.IfZeitpunkt;

/**
 * @author Kevin
 *
 */
public class MonatsEinnahmen extends AbstractEinnahmenCalculator
{

	@Override
	public double berechneEinnahmen(List<IfStatistik> werte, IfZeitpunkt zeitpunkt, int tag)
	{
		List<IfStatistik> monatWerte = super.getStatstikForZeitpunkt(werte, zeitpunkt);
		double wert = 0.;
		for (IfStatistik s : monatWerte)
		{
			wert += s.getBezahlterPreis();
		}
		return wert;
	}

}
