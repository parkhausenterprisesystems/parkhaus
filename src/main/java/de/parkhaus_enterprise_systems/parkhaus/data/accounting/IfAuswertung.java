/**
 * (c) 2017 Parkhaus Enterprise Systems
 */
package de.parkhaus_enterprise_systems.parkhaus.data.accounting;

/**
 * @author Kevin
 *
 */
public interface IfAuswertung
{
	IfZeitpunkt getZeitpunkt();

	double getEinnahmen();

	double getAusgaben();

	boolean hasGewinn();

	boolean hasVerlust();
}
