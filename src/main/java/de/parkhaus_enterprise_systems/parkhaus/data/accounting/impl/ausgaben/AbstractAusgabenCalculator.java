/**
 * (c) 2018 Parkhaus Enterprise Systems
 */
package de.parkhaus_enterprise_systems.parkhaus.data.accounting.impl.ausgaben;

import java.util.ArrayList;
import java.util.List;

import de.parkhaus_enterprise_systems.parkhaus.data.accounting.IfStatistik;
import de.parkhaus_enterprise_systems.parkhaus.data.accounting.IfZeitpunkt;

/**
 * @author Sascha
 *
 */
public abstract class AbstractAusgabenCalculator
{
	public abstract double berechneAusgaben(List<IfStatistik> werte, IfZeitpunkt zeitpunkt, int tag);

	protected List<IfStatistik> getStatstikForZeitpunkt(List<IfStatistik> werte, IfZeitpunkt z)
	{
		List<IfStatistik> k = new ArrayList<IfStatistik>();
		for (IfStatistik i : werte)
		{
			if (i != null && i.getZeitpunkt().equals(z))
			{
				k.add(i);
			}
		}
		return k;
	}
}
