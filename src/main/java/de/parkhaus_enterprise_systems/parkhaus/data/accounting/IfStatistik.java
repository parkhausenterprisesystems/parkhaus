/**
 * (c) 2017 Parkhaus Enterprise Systems
 */
package de.parkhaus_enterprise_systems.parkhaus.data.accounting;

import de.parkhaus_enterprise_systems.parkhaus.util.EnGeschlecht;

/**
 * @author Kevin
 *
 */
public interface IfStatistik
{
	int getEtageIndex();

	int getParkplatzIndex();

	String getKennzeichen();

	IfZeitpunkt getZeitpunkt();

	Long getEinfahrtszeit();

	Long getAusfahrtszeit();
	
	Long getOeffnungszeit();
	
	Long getSchliessungszeit();

	double getBezahlterPreis();

	String getFahrzeugFarbe();

	boolean hasFahrzeugbehinderung();

	EnGeschlecht getFahrzeugGeschlecht();

	int getTag();

}
