/**
 * (c) 2017 Parkhaus Enterprise Systems
 */
package de.parkhaus_enterprise_systems.parkhaus.data.accounting.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import de.parkhaus_enterprise_systems.parkhaus.data.IfParkhaus;
import de.parkhaus_enterprise_systems.parkhaus.data.accounting.IfAuswertung;
import de.parkhaus_enterprise_systems.parkhaus.data.accounting.IfControllingUnit;
import de.parkhaus_enterprise_systems.parkhaus.data.accounting.IfStatistik;
import de.parkhaus_enterprise_systems.parkhaus.data.accounting.IfZeitpunkt;
import de.parkhaus_enterprise_systems.parkhaus.data.accounting.impl.einnahmen.AbstractEinnahmenCalculator;
import de.parkhaus_enterprise_systems.parkhaus.data.mangement.IfManagementUnit;
import de.parkhaus_enterprise_systems.parkhaus.util.EnGeschlecht;
import de.parkhaus_enterprise_systems.parkhaus.util.TimeUtil;

/**
 * @author Kevin
 *
 */
public class ControllingUnit implements IfControllingUnit
{

	IfParkhaus p;
	IfManagementUnit m;
	private List<IfStatistik> statistik;

	public ControllingUnit(IfParkhaus parkhaus)
	{
		this.p = parkhaus;
		this.m = parkhaus.getManagementUnit();
		statistik = new ArrayList<IfStatistik>();
	}

	@Override
	public double getEinnahmen(IfZeitpunkt zeitpunkt)
	{
		double einnahmen = 0;
		List<IfStatistik> statstikInZeitraum = getStatstikForZeitpunkt(zeitpunkt);
		for (IfStatistik k : statstikInZeitraum)
		{
			double bezahlt = TimeUtil.getGeparkteStunden(k.getEinfahrtszeit(), k.getAusfahrtszeit())
					* k.getBezahlterPreis();
			if (TimeUtil.getGeparkteMinuten(k.getEinfahrtszeit(), k.getAusfahrtszeit()) > 0)
			{
				bezahlt += k.getBezahlterPreis();
			}
			einnahmen += bezahlt;
		}
		return einnahmen;
	}

	@Override
	public double berechneEinnahmen(IfZeitpunkt zeitpunkt, int tag, AbstractEinnahmenCalculator calculator)
	{
		return calculator.berechneEinnahmen(statistik, zeitpunkt, tag);
	}

	@Override
	public double getAusgaben(IfZeitpunkt zeitpunkt)
	{
		// AKTUELL WERDEN NOCH KEINE AUSGABEN GETRACKT
		double ausgaben = 0;
		List<IfStatistik> statstikInZeitraum = getStatstikForZeitpunkt(zeitpunkt);
		for (IfStatistik k : statstikInZeitraum)
		{
			double kosten = TimeUtil.getGeoeffneteZeit(k.getOeffnungszeit(), k.getSchliessungszeit())
					* m.getAusgabenProStunde();
			ausgaben += kosten;
		}
		return ausgaben;
	}

	@Override
	public double getEinnhamen()
	{
		return getEinnahmen(new Zeitpunkt());
	}

	@Override
	public double getAusgaben()
	{
		return getAusgaben(new Zeitpunkt());
	}

	@Override
	public double getGewinn(IfZeitpunkt zeitpunkt)
	{
		return getEinnahmen(zeitpunkt) - getAusgaben(zeitpunkt);
	}

	@Override
	public double getGewinn()
	{
		return getGewinn(new Zeitpunkt());
	}

	@Override
	public boolean hasGewinn()
	{
		return hasGewinn(new Zeitpunkt());
	}

	@Override
	public boolean hasVerlust()
	{
		return hasVerlust(new Zeitpunkt());
	}

	@Override
	public boolean hasGewinn(IfZeitpunkt zeitpunkt)
	{
		return getGewinn(zeitpunkt) > 0;
	}

	@Override
	public boolean hasVerlust(IfZeitpunkt zeitpunkt)
	{
		return getGewinn(zeitpunkt) < 0;
	}

	@Override
	public List<IfAuswertung> getAuswertung(IfZeitpunkt von, IfZeitpunkt bis)
	{
		List<IfAuswertung> auswertung = new ArrayList<IfAuswertung>();
		// TODO HIER FEHLT NOCH DIE IMPL.
		return auswertung;
	}

	@Override
	public int getAnzahlFahrzeuge(IfZeitpunkt zeitpunkt)
	{
		return getStatstikForZeitpunkt(zeitpunkt).size();
	}

	@Override
	public List<IfStatistik> getStatstikForZeitpunkt(IfZeitpunkt z)
	{
		List<IfStatistik> k = new ArrayList<IfStatistik>();
		for (IfStatistik i : this.statistik)
		{
			if (i != null && i.getZeitpunkt().equals(z))
			{
				k.add(i);
			}
		}
		return k;
	}

	@Override
	public List<IfStatistik> getStatstikInZeitraum(IfZeitpunkt von, IfZeitpunkt bis)
	{
		List<IfStatistik> k = new ArrayList<IfStatistik>();
		for (IfStatistik i : this.statistik)
		{
			if (isInZeitraum(i, von, bis))
			{
				k.add(i);
			}
		}
		return k;
	}

	private boolean isInZeitraum(IfStatistik k, IfZeitpunkt von, IfZeitpunkt bis)
	{
		IfZeitpunkt p = k.getZeitpunkt();
		if (von.getJahr() == bis.getJahr() && von.getMonat() == bis.getMonat() && p.getJahr() == von.getJahr()
				&& p.getMonat() == von.getMonat())
		{
			return true;
		}
		if (p.getJahr() >= von.getJahr() && p.getJahr() <= bis.getJahr())
		{
			if (p.getJahr() == von.getJahr() && p.getMonat() >= von.getMonat() && p.getJahr() < bis.getJahr())
			{
				return true;
			}
			if (p.getJahr() == bis.getJahr() && p.getMonat() <= bis.getMonat())
			{
				return true;
			}
			if (p.getJahr() > von.getJahr() && p.getJahr() < bis.getJahr())
			{
				return true;
			}
		}
		return false;
	}

	@Override
	public int getAnzahlFahrzeuge()
	{
		return getAnzahlFahrzeuge(new Zeitpunkt());
	}

	@Override
	public double getDurchschnittlicheParkzeit()
	{
		return getDurchschnittlicheParkzeit(new Zeitpunkt());
	}

	@Override
	public double getDurchschnittlicheParkzeit(IfZeitpunkt zeitpunkt)
	{
		// TODO Auto-generated method stub
		return 0;
	}

	/*
	 * return durchschnittliche Etage
	 */

	public static int calcMeanEtage(List<IfStatistik> stat)
	{
		int sum = 0;
		int cnt = 0;
		for (IfStatistik i : stat)
		{
			sum += i.getEtageIndex();
			cnt++;
		}
		return (sum / cnt);
	}

	/*
	 * return durchschnittlicher Parkplatz
	 */

	public static int calcMeanParkplatz(List<IfStatistik> stat)
	{
		int sum = 0;
		int cnt = 0;
		for (IfStatistik i : stat)
		{
			sum += i.getParkplatzIndex();
			cnt++;
		}
		return (sum / cnt);
	}

	// return Durchschnittswert zwischen 0 und 1 (nur Frauen - nur Männer)

	public static double calcMeanGeschlecht(List<IfStatistik> stat)
	{

		double sum = 0;
		int cnt = 0;
		for (IfStatistik i : stat)
		{
			if (i.getFahrzeugGeschlecht().equals(EnGeschlecht.MAENNLICH))
			{
				sum++;
			}
			cnt++;
		}
		sum = sum / cnt;
		return sum;

	}

	// return Durchschnittswert zwischen 0 und 1 (nur nicht behindert - nur
	// behindert)

	public static double calcMeanBehinderung(List<IfStatistik> stat)
	{

		double sum = 0;
		int cnt = 0;
		for (IfStatistik i : stat)
		{
			if (i.hasFahrzeugbehinderung())
			{
				sum++;
			}
			cnt++;
		}
		sum = sum / cnt;
		return sum;

	}

	public static double calcMeanEinfahrtszeit(List<IfStatistik> stat)
	{

		double sum = 0;
		int cnt = 0;
		for (IfStatistik i : stat)
		{
			sum += i.getEinfahrtszeit();
			cnt++;
		}

		sum = sum / cnt;
		return sum;
	}

	public static double calcMeanAusfahrtszeit(List<IfStatistik> stat)
	{

		double sum = 0;
		int cnt = 0;
		for (IfStatistik i : stat)
		{
			sum += i.getAusfahrtszeit();
			cnt++;
		}

		sum = sum / cnt;
		return sum;
	}

	public static double calcMeanParkkosten(List<IfStatistik> stat)
	{

		double sum = 0;
		int cnt = 0;
		for (IfStatistik i : stat)
		{
			sum += i.getBezahlterPreis();
			cnt++;
		}

		sum = sum / cnt;
		return sum;
	}

	// return am meisten benutzte Etage
	public static int calcMostUsedEtage(List<IfStatistik> stat)
	{

		int[] etagen = new int[stat.size()];

		for (int i = 0; i < stat.size(); i++)
		{
			IfStatistik s = stat.get(i);
			etagen[i] = s.getEtageIndex();
		}
		Arrays.sort(etagen);

		int found = etagen[0];
		int foundLen = findLen(etagen, 0);

		for (int i = 1; i < etagen.length;)
		{
			int actLen = findLen(etagen, i);
			if (actLen > foundLen)
			{
				found = etagen[i];
				foundLen = actLen;
			}
			i = i + actLen;
		}

		return found;

	}

	// return am meisten benutzter Parkplatz

	public static int calcMostUsedParkplatz(List<IfStatistik> stat)
	{

		int[] park = new int[stat.size()];

		for (int i = 0; i < stat.size(); i++)
		{
			IfStatistik s = stat.get(i);
			park[i] = s.getEtageIndex();
		}
		Arrays.sort(park);

		int found = park[0];
		int foundLen = findLen(park, 0);

		for (int i = 1; i < park.length;)
		{
			int actLen = findLen(park, i);
			if (actLen > foundLen)
			{
				found = park[i];
				foundLen = actLen;
			}
			i = i + actLen;
		}

		return found;

	}

	// Hilfsmethode für calcMostUsedX
	private static int findLen(int[] alle, int startPos)
	{
		int startElement = alle[startPos];
		int len = 1;
		for (int i = startPos + 1; i < alle.length; i++)
		{
			if (alle[i] != startElement)
			{
				return len;
			}
			len++;
		}

		return len;
	}

	public static double kovarianzGeschlechtBehinderung(List<IfStatistik> stat)
	{

		double geschlechtDurchschnitt = calcMeanGeschlecht(stat);
		double behinderungDurchschnitt = calcMeanBehinderung(stat);
		double sum = 0;
		for (IfStatistik i : stat)
		{
			double ge = 0;
			double be = 0;
			if (i.getFahrzeugGeschlecht().equals(EnGeschlecht.MAENNLICH))
			{
				ge = 1;
			}
			if (i.hasFahrzeugbehinderung())
			{
				be = 1;
			}
			sum += (ge - geschlechtDurchschnitt) * (be - behinderungDurchschnitt);
		}
		return sum / (stat.size() - 1);
	}

	public static double kovarianzEinfahrtAusfahrt(List<IfStatistik> stat)
	{

		double einfahrtDurchschnitt = calcMeanEinfahrtszeit(stat);
		double ausfahrtDurchschnitt = calcMeanAusfahrtszeit(stat);
		double sum = 0;
		for (IfStatistik i : stat)
		{
			sum += (i.getEinfahrtszeit() - einfahrtDurchschnitt) * (i.getAusfahrtszeit() - ausfahrtDurchschnitt);
		}
		return sum / (stat.size() - 1);
	}

	public static double kovarianzGeschlechtParkkosten(List<IfStatistik> stat)
	{

		double geschlechtDurchschnitt = calcMeanGeschlecht(stat);
		double parkkostenDurchschnitt = calcMeanParkkosten(stat);
		double sum = 0;
		for (IfStatistik i : stat)
		{
			double ge = 0;
			if (i.getFahrzeugGeschlecht().equals(EnGeschlecht.MAENNLICH))
			{
				ge = 1;
			}
			sum += (ge - geschlechtDurchschnitt) * (i.getBezahlterPreis() - parkkostenDurchschnitt);
		}
		return sum / (stat.size() - 1);
	}

	@Override
	public void addStatistik(IfStatistik stat)
	{
		if (stat != null)
		{
			statistik.add(stat);
		}
	}

	@Override
	public List<IfStatistik> getStatistik()
	{
		return statistik;
	}

}
