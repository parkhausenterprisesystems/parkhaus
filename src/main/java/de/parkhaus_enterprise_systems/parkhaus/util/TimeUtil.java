/**
 * (c) 2017 Parkhaus Enterprise Systems
 */
package de.parkhaus_enterprise_systems.parkhaus.util;

/**
 * @author Kevin
 *
 */
public class TimeUtil
{
	public static long getGeoeffneteZeit(long oeffnen, long schliessen) {
		return schliessen - oeffnen;
	}

	public static long berechneParkzeit(long ankunft, long ausfahrt)
	{
		return ausfahrt - ankunft;
	}

	public static int getGeparkteStunden(Long einfahrt, Long ausfahrt)
	{
		Long dif = ausfahrt - einfahrt;
		int diffHrs = (int) Math.floor((dif % 86400000) / 3600000); // hours
		return diffHrs;
	}

	public static int getGeparkteMinuten(Long einfahrt, Long ausfahrt)
	{
		Long dif = ausfahrt - einfahrt;
		int diffMins = Math.round(((dif % 86400000) % 3600000) / 60000); // minutes

		return diffMins;
	}
}
