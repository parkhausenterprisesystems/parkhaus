/**
 * (c) 2017 Parkhaus Enterprise Systems
 */
package de.parkhaus_enterprise_systems.parkhaus.util;

/**
 * @author Kevin
 *
 */
public enum EnGeschlecht
{
 WEIBLICH,MAENNLICH
}
