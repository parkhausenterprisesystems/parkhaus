/**
 * (c) 2017 Parkhaus Enterprise Systems
 */
package de.parkhaus_enterprise_systems.parkhaus.util;

import java.util.Calendar;
import java.util.Random;

import de.parkhaus_enterprise_systems.parkhaus.data.IfKfz;
import de.parkhaus_enterprise_systems.parkhaus.data.IfParkhaus;
import de.parkhaus_enterprise_systems.parkhaus.data.accounting.IfZeitpunkt;
import de.parkhaus_enterprise_systems.parkhaus.data.accounting.impl.Zeitpunkt;
import de.parkhaus_enterprise_systems.parkhaus.data.impl.Kfz;

/**
 * @author Sascha
 *
 */
public class ZufallsgeneratorUtil
{
	public static String[] farben = new String[]
	{ "#f44336", "#9c27b0", "#3f51b5", "#2196f3", "#03a9f4", "#009688", "#4caf50", "#ffc107", "#795548", "#607d8b",
			"#212121", "#fafafa" };

	public static String generiereKennzeichen()
	{
		Random rand = new Random();

		int zahl = rand.nextInt(9999) + 1;
		char[] c = new char[4];

		for (int i = 0; i < c.length; i++)
		{
			c[i] = (char) (rand.nextInt(25) + 65);
		}

		return c[0] + "" + c[1] + "-" + c[2] + "" + c[3] + "-" + zahl;

	}

	public static IfKfz generiereFahrzeug(IfParkhaus parkhaus)
	{
		String kennzeichen = "";
		do
		{
			kennzeichen = generiereKennzeichen();
		} while (parkhaus.hasKennzeichen(kennzeichen));
		String color = getRandomColor();
		EnGeschlecht geschlecht = getRandomGeschlecht();
		Boolean behinderung = getRandowmBehinderung();
		IfKfz kfz = new Kfz(kennzeichen, geschlecht, color, behinderung);
		return kfz;
	}

	public static String getRandomColor()
	{
		Random rand = new Random();
		int zahl = rand.nextInt(farben.length);
		if (zahl >= 0 && zahl < farben.length)
		{
			return farben[zahl];
		}
		return "";
	}

	public static EnGeschlecht getRandomGeschlecht()
	{
		Random rand = new Random();
		int zahl = rand.nextInt(EnGeschlecht.values().length);
		if (zahl >= 0 && zahl < EnGeschlecht.values().length)
		{
			return EnGeschlecht.values()[zahl];
		}
		return EnGeschlecht.MAENNLICH;
	}

	public static Boolean getRandowmBehinderung()
	{
		Random rand = new Random();
		return rand.nextInt(10) >= 8;
	}

	public static IfZeitpunkt getRandomZeitpunkt(int jahr)
	{
		Random rand = new Random();
		IfZeitpunkt z = new Zeitpunkt(rand.nextInt(11) + 1, jahr);
		return z;
	}

	public static int getRandomTag(IfZeitpunkt zeitpunkt)
	{
		Calendar instance = Calendar.getInstance();
		instance.set(zeitpunkt.getJahr(), zeitpunkt.getMonat() - 1, 1);
		int max = Calendar.getInstance().getMaximum(Calendar.DATE);
		Random rand = new Random();
		return rand.nextInt(max - 1) + 1;
	}
}
