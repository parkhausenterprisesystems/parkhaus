# Lastenheft #
## Funkton eines Parkhauses (Funktionale Anforderungen)##
	1. Das Abrechnungssystem des digitalisierten Parkhauses muss dem Kassierer die Möglichkeit bieten, die Parkgebühren nach Ablauf der Parkzeit zu kassieren.
	2. Das Wärtersystem muss dem Schrankenwärter die Möglichkeit bieten, die Einfahrt weiterer Fahrzeige zu verhindern, falls nicht genügend freie Parkplätze vorhanden sind.
	3. Das Wärtersystem muss dem Schrankenwärter die Möglichkeit bieten, die Ausfahrt von Fahrzeugen zu verhindern, falls der entsprechende Fahrzeugführer die Parkgebühr nicht bezahlt hat.
	4. Das Wärtersystem sollte dem Wärter die Möglichkeit bieten, unterschiedliche Typen von Fahrzeugführern an verschiedene Arten von Parkplätzen zu leiten.
	5. Das Wärtersystem sollte fähig sein, Kennzeichen von einfahrenden Fahrzeugen zu identifizieren, und diese für die Parkdauer abzuspeichern.
	6. Das Einweisungssystem muss dem Parkplatzeinweiser die Option bieten, alle noch nicht belegten Parkplätze anzuzeigen und einfahrende Fahrzeuge zu einem freien Stellplatz zu leiten.
	7. Das Einweisungssystem sollte dem Einweiser die Möglichkeit bieten, mehr als eine Etage zu überblicken bzw. zu verwalten.
	8. Das Managementsystem muss fähig sein, alle Daten die die Betriebsabläufe, Öffnungszeiten, Regeln, Preise und freie Parkplätze betreffen, anzuzeigen.
	9. Das Managementsystem muss fähig sein, den Kassenbestand in einem beliebigen Zeitraum anzuzeigen, wobei die Daten in einer Kurve dargestellt werden.
	10. Das Managementsystem sollte dem Manager die Möglichkeit bieten, Daten wie Preise, Öffnungszeiten und Regeln zu ändern. 
	
##Nicht-funktionale Anforderungen##
	1. Das Parkhaussystem sollte eine grafische Benutzeroberfläche aufweisen
	
##Technische Anforderungen##
	1. Das Parkhaussystem muss lauffähig sein.
	2. Das Parkhaussystem sollte über eine Datenbankanbindung verfügen.
	
##Rahmenbedingungen##
	1.  zunächst keine

