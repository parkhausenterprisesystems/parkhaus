# Wer sind die Stakeholder eines digitalisierten Parkhauses? #
## Intern ##
 - Mitarbeiter (aktiv)
 - Manager	(aktiv)
 - Eigentümer (passiv)
 - Systembetreiber (aktiv)
 
## Extern ##
 - Kunden (PKW mit Fahrer) (aktiv)
 
# Welche Konzepte für ein digitalisiertes Parkhaus stiftet welchen Nutzen für welche Stakeholder? #
 - der Eigentümer muss weniger Mitarbeiter einstellen und kann somit mehr Gewinn aus dem Unternehmen ziehen.
 - Der Manager hat alle Informationen auf einen Blick und kann somit den Prozess besser optimieren und etvl. früher Probleme erkennen.
 - Der Mitarbeiter (wenn es noch welche gibt) muss weniger aber wichtigere Aufgaben erledigen. Mehr Verantwortung.
 - Der Systembetreiber muss mehr Verantwortung übernehmen, da alle Komponenten funktionieren müssen.
 - der Kunde wird besser über die freien Parkplätze informiert.
 
# Welche Konzepte für ein digitalisiertes Parkhaus verbessert das Leben welcher Stakeholder? Was wird für wen (a) einfacher (b) effizienter, produktiver (c) angenehmer? #
## (a) Einfacher: ##
 - Für den Eigentümer, da er weniger Aufwand in Buchhaltung usw investieren muss.
 - Für den Manager, da er alle Zahlen und Statistiken auf einen Blick sichtbar hat.
 
## (b) Effizienter: ##
 - Für den Eigentümer, da er mehr Gewinn machen kann und sich weniger kümmern muss.
 - Für den Kunden, da er einen besseren Überblick über die Parkplatzsituation hat.
 - Für den Manager, da er das Parkhaus besser steuern kann.

## (c) Produktiver: ##
 - Für den Mitarbeiter, da er keine einfachen Tätigkeiten mehr ausführen muss und somit produktiver Arbeiten kann.

# Welche Konzepte für Ihr digitalisiertes Parkhaus sind disruptiv? #
 Vorallem für die Mitarbeiter werden folgende Aufgaben völlig verdrängt:
 - Schrankenwärter
 - Buchhaltung
 - Berechnung der Parkzeit
 - Parkeinweiser
 Außerdem entfallen auch Aufgaben des Managers.
 
# Welches Ihrer Konzepte für eine "bessere Welt" erfordert welchen Kommunikationsplan mit welchem Stakeholder? #
