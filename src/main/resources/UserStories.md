# User Stories #
	1. Als *Kunde* _benötige_ ich *einen Parkplatz*, um *mein Auto abstellen* zu können.
	2. Als *Kunde* _benötige_ ich *ein Kennzeichen*, um *identifiziert werden* zu können.
	3. Als *Kunde* _benötige_ ich *einen Parkschein*, um *in das Parkhaus einfahren* zu können.
	4. Als *Kunde* _möchte_ ich *einen freien Parkplatz zugewiesen bekommen*, um *mein Auto dort abstellen* zu können.
	5. Als *Kunde* _möchte_ ich *einen Parkplatz*, um *mein Auto abstellen* zu können.
	6. Als *Kunde* _möchte_ ich *meine Parkkosten sehen*, um *das Parkticket bezahlen* zu können.
	7. Als *Kunde* _möchte_ ich *meinen Parkschein bezahlen*, um *das Parkhaus verlassen* zu können.

	8. Als *Manager* _möchte_ ich *die monatlichen Kunden sehen*, um *einen Überblick behalten* zu können.
	9. Als *Manager* _möchte_ ich *die monatlichen Einnahmen sehen*, um *eine Buchhaltung erstellen* zu können.
