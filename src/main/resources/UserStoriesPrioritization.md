# User Stories Prioritization#

| User Story | Aufwand | Mehrwert | Strafe | Risiko |
|------------|---------|----------|--------|--------|
| US 1       | 5       | 10       | 7      | 1      |
| US 2       | 6       | 10       | 8      | 1      |
| US 3       | 4       | 8        | 5      | 2      |
| US 4       | 6       | 8        | 5      | 2      |
| US 5       | 5       | 10       | 8      | 1      |
| US 6       | 5       | 9        | 10     | 1      |
| US 7       | 6       | 10       | 5      | 1      |
| US 8       | 8       | 10       | 7      | 1      |
| US 9       | 8       | 10       | 7      | 1      |