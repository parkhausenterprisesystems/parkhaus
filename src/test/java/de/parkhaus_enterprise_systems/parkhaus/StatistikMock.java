/**
 * (c) 2018 Parkhaus Enterprise Systems
 */
package de.parkhaus_enterprise_systems.parkhaus;

import de.parkhaus_enterprise_systems.parkhaus.data.IfKfz;
import de.parkhaus_enterprise_systems.parkhaus.data.accounting.IfZeitpunkt;
import de.parkhaus_enterprise_systems.parkhaus.data.accounting.impl.Statistik;
import de.parkhaus_enterprise_systems.parkhaus.data.mangement.IfManagementUnit;

/**
 * @author Kevin
 *
 */
public class StatistikMock extends Statistik
{

	public StatistikMock(int etagenIndex, int parkplatzIndex, IfKfz kfz, IfManagementUnit manage, IfZeitpunkt zeitpunkt,
			int tag)
	{
		super(etagenIndex, parkplatzIndex, kfz, manage);
		super.zeitpunt = zeitpunkt;
		super.tag = tag;
	}

	@Override
	public double getBezahlterPreis()
	{
		return this.manage.getPreisProStunde();
	}

}
