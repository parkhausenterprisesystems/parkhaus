/**
 * (c) 2017 Parkhaus Enterprise Systems
 */
package de.parkhaus_enterprise_systems.parkhaus;

import de.parkhaus_enterprise_systems.parkhaus.data.IfEtage;
import de.parkhaus_enterprise_systems.parkhaus.data.IfKfz;
import de.parkhaus_enterprise_systems.parkhaus.data.accounting.IfZeitpunkt;
import de.parkhaus_enterprise_systems.parkhaus.data.impl.Parkhaus;
import de.parkhaus_enterprise_systems.parkhaus.data.mangement.impl.ManagementUnit;

/**
 * @author Kevin
 *
 */
public class ParkhausMock extends Parkhaus
{

	public ParkhausMock()
	{
		super(5, 10, 5, 5, new ManagementUnit(1.2));
	}

	public boolean MockFahrzeugAusfahren(IfKfz fahrzeug, IfZeitpunkt zeitpunkt, int tag)
	{
		for (int i = 0; i < super.getEtagen().size(); i++)
		{
			IfEtage e = this.getEtagen().get(i);
			int pIndex = e.fahrzeugAusfahren(fahrzeug);
			if (pIndex >= 0)
			{
				StatistikMock k = new StatistikMock(i, pIndex, fahrzeug, this.getManagementUnit(), zeitpunkt, tag);
				this.getControllingUnit().addStatistik(k);
				this.kennzeichenliste.remove(fahrzeug.getKennzeichen());
				return true;
			}
		}
		return false;
	}

}
