/**
 * (c) 2018 Parkhaus Enterprise Systems
 */
package de.parkhaus_enterprise_systems.parkhaus;

import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

import de.parkhaus_enterprise_systems.parkhaus.data.IfKfz;
import de.parkhaus_enterprise_systems.parkhaus.data.accounting.IfZeitpunkt;
import de.parkhaus_enterprise_systems.parkhaus.data.accounting.impl.Zeitpunkt;
import de.parkhaus_enterprise_systems.parkhaus.data.accounting.impl.einnahmen.AbstractEinnahmenCalculator;
import de.parkhaus_enterprise_systems.parkhaus.data.accounting.impl.einnahmen.JahresEinnahmen;
import de.parkhaus_enterprise_systems.parkhaus.data.accounting.impl.einnahmen.MonatsEinnahmen;
import de.parkhaus_enterprise_systems.parkhaus.data.accounting.impl.einnahmen.TagesEinnahmen;
import de.parkhaus_enterprise_systems.parkhaus.util.ZufallsgeneratorUtil;

/**
 * @author Kevin
 *
 */
public class EinnahmenTemplateTest
{
	ParkhausMock parkhaus_1;

	@Before
	public void startUp()
	{
		parkhaus_1 = new ParkhausMock();
		for (int i = 0; i < 10; i++)
		{
			IfKfz k = ZufallsgeneratorUtil.generiereFahrzeug(parkhaus_1);
			parkhaus_1.fahrzeugEinfahren(k);
			IfZeitpunkt z = new Zeitpunkt(1, 2018);
			parkhaus_1.MockFahrzeugAusfahren(k, z, 10);
		}
		for (int i = 0; i < 10; i++)
		{
			IfKfz k = ZufallsgeneratorUtil.generiereFahrzeug(parkhaus_1);
			parkhaus_1.fahrzeugEinfahren(k);
			IfZeitpunkt z = new Zeitpunkt(1, 2018);
			parkhaus_1.MockFahrzeugAusfahren(k, z, 9);
		}
		for (int i = 0; i < 10; i++)
		{
			IfKfz k = ZufallsgeneratorUtil.generiereFahrzeug(parkhaus_1);
			parkhaus_1.fahrzeugEinfahren(k);
			IfZeitpunkt z = new Zeitpunkt(2, 2018);
			parkhaus_1.MockFahrzeugAusfahren(k, z, 12);
		}
	}

	@Test
	public void AnzahlStatistikTest()
	{
		double richtigerWert = 30;
		assertTrue(richtigerWert == parkhaus_1.getControllingUnit().getStatistik().size());
	}

	@Test
	public void TagesTest()
	{
		double richtigerWert = 10 * parkhaus_1.getManagementUnit().getPreisProStunde();
		AbstractEinnahmenCalculator monatlicheEinnamhen = new TagesEinnahmen();
		double errechneterWert = monatlicheEinnamhen.berechneEinnahmen(parkhaus_1.getControllingUnit().getStatistik(),
				new Zeitpunkt(1, 2018), 10);
		assertTrue(errechneterWert <= richtigerWert + 1 && errechneterWert >= richtigerWert - 1);
	}

	@Test
	public void MonatsTest()
	{
		double richtigerWert = 20 * parkhaus_1.getManagementUnit().getPreisProStunde();
		AbstractEinnahmenCalculator monatlicheEinnamhen = new MonatsEinnahmen();
		double errechneterWert = monatlicheEinnamhen.berechneEinnahmen(parkhaus_1.getControllingUnit().getStatistik(),
				new Zeitpunkt(1, 2018), -1);
		assertTrue(errechneterWert <= richtigerWert + 1 && errechneterWert >= richtigerWert - 1);
	}

	@Test
	public void JahresTest()
	{
		double richtigerWert = 30 * parkhaus_1.getManagementUnit().getPreisProStunde();
		AbstractEinnahmenCalculator monatlicheEinnamhen = new JahresEinnahmen();
		double errechneterWert = monatlicheEinnamhen.berechneEinnahmen(parkhaus_1.getControllingUnit().getStatistik(),
				new Zeitpunkt(1, 2018), -1);
		assertTrue(errechneterWert <= richtigerWert + 1 && errechneterWert >= richtigerWert - 1);
	}

}
