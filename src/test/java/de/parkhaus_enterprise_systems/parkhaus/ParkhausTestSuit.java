package de.parkhaus_enterprise_systems.parkhaus;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

import de.parkhaus_enterprise_systems.parkhaus.data.IfEtage;
import de.parkhaus_enterprise_systems.parkhaus.data.IfKfz;
import de.parkhaus_enterprise_systems.parkhaus.data.IfParkhaus;
import de.parkhaus_enterprise_systems.parkhaus.data.impl.Kfz;
import de.parkhaus_enterprise_systems.parkhaus.data.impl.Parkhaus;
import de.parkhaus_enterprise_systems.parkhaus.util.EnGeschlecht;
import de.parkhaus_enterprise_systems.parkhaus.util.EnParkplatz;
import de.parkhaus_enterprise_systems.parkhaus.util.ZufallsgeneratorUtil;

public class ParkhausTestSuit
{

	// DIESES PARKHAUS HAT KEINE PARKPLÄTZE
	IfParkhaus parkhaus_1;
	// DIESES PARKHAUS HAT 2 ETAGEN UND 10 PARKPLÄTZE PRO ETAGE
	IfParkhaus parkhaus_2;

	@Before
	public void startUp()
	{
		parkhaus_1 = new Parkhaus();
		parkhaus_2 = new Parkhaus(2, 10);
	}

	@Test
	public void Parkhaus_kennzeichenTest_1()
	{
		String kennzeichen = ZufallsgeneratorUtil.generiereKennzeichen();
		assertFalse(parkhaus_1.hasKennzeichen(kennzeichen));
	}

	@Test
	public void Parkhaus_kennzeichenTest_2()
	{
		String kennzeichen = ZufallsgeneratorUtil.generiereKennzeichen();
		IfKfz fahrzeug = new Kfz(kennzeichen);
		parkhaus_1.fahrzeugEinfahren(fahrzeug);
		assertTrue(parkhaus_1.hasKennzeichen(kennzeichen));
	}

	@Test
	public void Parkhaus_kennzeichenTest_3()
	{
		String kennzeichen = ZufallsgeneratorUtil.generiereKennzeichen();
		IfKfz fahrzeug = new Kfz(kennzeichen);
		parkhaus_2.fahrzeugEinfahren(fahrzeug);
		assertTrue(parkhaus_2.hasKennzeichen(kennzeichen));
	}

	@Test
	public void Parkhaus_einfahrtTest_1()
	{
		assertFalse(parkhaus_1.fahrzeugEinfahren(null));
	}

	@Test
	public void Parkhaus_einfahrtTest_2()
	{
		String kennzeichen = ZufallsgeneratorUtil.generiereKennzeichen();
		IfKfz fahrzeug = new Kfz(kennzeichen);
		assertTrue(parkhaus_2.fahrzeugEinfahren(fahrzeug));
	}

	@Test
	public void Parkhaus_einfahrtTest_3()
	{
		String kennzeichen = ZufallsgeneratorUtil.generiereKennzeichen();
		IfKfz fahrzeug = new Kfz(kennzeichen);
		assertTrue(parkhaus_2.fahrzeugEinfahren(fahrzeug));
		assertFalse(parkhaus_2.fahrzeugEinfahren(fahrzeug));
	}

	@Test
	public void Parkhaus_parkplatzAnzahlTest_1()
	{
		int freieParkplaetzeBefore = parkhaus_2.getFreieParkplaetze(EnParkplatz.NORMAL);
		IfKfz fahrzeug = getDefaultFahrzeug();
		parkhaus_2.fahrzeugEinfahren(fahrzeug);
		int freieParkplaetzeAfter = parkhaus_2.getFreieParkplaetze(EnParkplatz.NORMAL);
		assertEquals(freieParkplaetzeBefore, freieParkplaetzeAfter + 1);
	}

	@Test
	public void Parkhaus_parkplatzAnzahlTest_2()
	{
		int freieParkplaetzeBefore = parkhaus_2.getFreieParkplaetze(EnParkplatz.NORMAL);
		IfKfz fahrzeug = getDefaultFahrzeug();
		parkhaus_2.fahrzeugEinfahren(fahrzeug);
		int freieParkplaetzeAfter = parkhaus_2.getFreieParkplaetze(EnParkplatz.NORMAL);
		assertEquals(freieParkplaetzeBefore, freieParkplaetzeAfter + 1);
		parkhaus_2.fahrzeugAusfahren(fahrzeug);
		int freieParkplaetzeNachAusfahrt = parkhaus_2.getFreieParkplaetze(EnParkplatz.NORMAL);
		assertEquals(freieParkplaetzeBefore, freieParkplaetzeNachAusfahrt);

	}

	@Test
	public void Parkhaus_parklatzAnzahlTest_2()
	{
		int freieParkplaetze = parkhaus_2.getFreieParkplaetze(EnParkplatz.NORMAL);
		assertTrue(freieParkplaetze >= 0);
	}

	@Test
	public void Parkhaus_parklatzAnzahlTest_3()
	{
		int freieParkplaetze = parkhaus_1.getFreieParkplaetze(EnParkplatz.NORMAL);
		assertTrue(freieParkplaetze == 0);
	}

	@Test
	public void Parkhaus_parklatzAnzahlTest_4()
	{
		int freieParkplaetze = parkhaus_1.getAnzahlParkplaetze(null);
		assertEquals(-1, freieParkplaetze);
	}

	@Test
	public void Parkhaus_parklatzAnzahlTest_5()
	{
		int freieParkplaetze = parkhaus_1.getAnzahlParkplaetze(EnParkplatz.NORMAL);
		assertEquals(0, freieParkplaetze);
	}

	@Test
	public void Parkhaus_EtagenTest_Kennzeichen_1()
	{
		IfKfz fahrzeug = getDefaultFahrzeug();
		parkhaus_2.fahrzeugEinfahren(fahrzeug);
		IfEtage etage = parkhaus_2.getEtagen().get(0);
		etage.hasKennzeichen(fahrzeug.getKennzeichen());
	}

	@Test
	public void Parkhaus_EtagenTest_Kennzeichen_2()
	{
		IfKfz fahrzeug = getDefaultFahrzeug();
		parkhaus_2.fahrzeugEinfahren(fahrzeug);
		IfKfz kfz = parkhaus_2.getFirstKfz();
		assertEquals(kfz.getKennzeichen(), fahrzeug.getKennzeichen());
	}

	IfKfz getDefaultFahrzeug()
	{
		return new Kfz(ZufallsgeneratorUtil.generiereKennzeichen(), EnGeschlecht.MAENNLICH, "Rot", false);
	}

	IfKfz getFahrzeugFrau()
	{
		return new Kfz(ZufallsgeneratorUtil.generiereKennzeichen(), EnGeschlecht.WEIBLICH, "Rot", false);
	}

	IfKfz getFahrzeugBehinderug()
	{
		return new Kfz(ZufallsgeneratorUtil.generiereKennzeichen(), EnGeschlecht.MAENNLICH, "Rot", true);
	}
}
