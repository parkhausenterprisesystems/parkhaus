/**
 * (c) 2017 Parkhaus Enterprise Systems
 */
package de.parkhaus_enterprise_systems.parkhaus;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

import de.parkhaus_enterprise_systems.parkhaus.data.IfKfz;
import de.parkhaus_enterprise_systems.parkhaus.data.IfParkhaus;
import de.parkhaus_enterprise_systems.parkhaus.data.impl.Kfz;
import de.parkhaus_enterprise_systems.parkhaus.util.EnGeschlecht;
import de.parkhaus_enterprise_systems.parkhaus.util.EnParkplatz;
import de.parkhaus_enterprise_systems.parkhaus.util.ZufallsgeneratorUtil;

/**
 * @author Kevin
 *
 */
public class MockTestSuite
{
	IfParkhaus parkhaus;

	@Before
	public void startUp()
	{
		parkhaus = new ParkhausMock();
	}

	@Test
	public void Parkhaus_kennzeichenTest_1()
	{
		String kennzeichen = ZufallsgeneratorUtil.generiereKennzeichen();
		assertFalse(parkhaus.hasKennzeichen(kennzeichen));
	}

	@Test
	public void Parkhaus_einfahrtTest_1()
	{
		IfKfz kfz_1 = getDefaultFahrzeug();
		parkhaus.fahrzeugEinfahren(kfz_1);
		assertTrue(parkhaus.hasKennzeichen(kfz_1.getKennzeichen()));
	}

	@Test
	public void Parkhaus_einfahrtTest_2()
	{
		int freieParkplaetzeBefore = parkhaus.getFreieParkplaetze(EnParkplatz.NORMAL);
		IfKfz kfz_1 = getDefaultFahrzeug();
		parkhaus.fahrzeugEinfahren(kfz_1);
		assertTrue(parkhaus.getFreieParkplaetze(EnParkplatz.NORMAL) < freieParkplaetzeBefore);
	}

	IfKfz getDefaultFahrzeug()
	{
		return new Kfz(ZufallsgeneratorUtil.generiereKennzeichen(), EnGeschlecht.MAENNLICH, "Rot", false);
	}
}
