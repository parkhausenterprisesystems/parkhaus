/**
 * (c) 2017 Parkhaus Enterprise Systems
 */
package de.parkhaus_enterprise_systems.parkhaus;

import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

import de.parkhaus_enterprise_systems.parkhaus.data.IfKfz;
import de.parkhaus_enterprise_systems.parkhaus.data.IfParkhaus;
import de.parkhaus_enterprise_systems.parkhaus.data.impl.Parkhaus;
import de.parkhaus_enterprise_systems.parkhaus.data.mangement.impl.ManagementUnit;
import de.parkhaus_enterprise_systems.parkhaus.util.EnParkplatz;
import de.parkhaus_enterprise_systems.parkhaus.util.ZufallsgeneratorUtil;

/**
 * @author Kevin
 *
 */
public class ParkhausBelegtTest
{
	IfParkhaus parkhaus_1 = new Parkhaus(5, 20, 0, 0, new ManagementUnit(1.0));

	@Before
	public void startUp()
	{
	}

	@Test
	public void checkPlaces()
	{
		assertTrue(parkhaus_1.getFreieParkplaetze(EnParkplatz.NORMAL) == 100);
	}

	@Test
	public void addCar()
	{
		IfKfz k = ZufallsgeneratorUtil.generiereFahrzeug(parkhaus_1);
		assertTrue(parkhaus_1.fahrzeugEinfahren(k));
		assertTrue(parkhaus_1.getFreieParkplaetze(EnParkplatz.NORMAL) == 99);
	}

}
