# README #

Restful service for this Project: https://bitbucket.org/parkhausenterprisesystems/parkhaus-restful-service/overview  
Fronted for this Project: https://bitbucket.org/parkhausenterprisesystems/parkhaus-frontend/overview  

## Current Build of Application ##
You can test thecurrent build of the web application under the following link.  
https://www.ultimapp.de/parkhaus/app

## JavaDoc ##
You can access the current JavaDoc at  https://ultimapp.de/parkhaus/apidocs/

# Lastenheft #
## Funkton eines Parkhauses (Funktionale Anforderungen)##
	1. Das Abrechnungssystem des digitalisierten Parkhauses muss dem Kassierer die Möglichkeit bieten, die Parkgebühren nach Ablauf der Parkzeit zu kassieren.
	2. Das Wärtersystem muss dem Schrankenwärter die Möglichkeit bieten, die Einfahrt weiterer Fahrzeige zu verhindern, falls nicht genügend freie Parkplätze vorhanden sind.
	3. Das Wärtersystem muss dem Schrankenwärter die Möglichkeit bieten, die Ausfahrt von Fahrzeugen zu verhindern, falls der entsprechende Fahrzeugführer die Parkgebühr nicht bezahlt hat.
	4. Das Wärtersystem sollte dem Wärter die Möglichkeit bieten, unterschiedliche Typen von Fahrzeugführern an verschiedene Arten von Parkplätzen zu leiten.
	5. Das Wärtersystem sollte fähig sein, Kennzeichen von einfahrenden Fahrzeugen zu identifizieren, und diese für die Parkdauer abzuspeichern.
	6. Das Einweisungssystem muss dem Parkplatzeinweiser die Option bieten, alle noch nicht belegten Parkplätze anzuzeigen und einfahrende Fahrzeuge zu einem freien Stellplatz zu leiten.
	7. Das Einweisungssystem sollte dem Einweiser die Möglichkeit bieten, mehr als eine Etage zu überblicken bzw. zu verwalten.
	8. Das Managementsystem muss fähig sein, alle Daten die die Betriebsabläufe, Öffnungszeiten, Regeln, Preise und freie Parkplätze betreffen, anzuzeigen.
	9. Das Managementsystem muss fähig sein, den Kassenbestand in einem beliebigen Zeitraum anzuzeigen, wobei die Daten in einer Kurve dargestellt werden.
	10. Das Managementsystem sollte dem Manager die Möglichkeit bieten, Daten wie Preise, Öffnungszeiten und Regeln zu ändern. 
	
##Nicht-funktionale Anforderungen##
	1. Das Parkhaussystem sollte eine grafische Benutzeroberfläche aufweisen
	
##Technische Anforderungen##
	1. Das Parkhaussystem muss lauffähig sein.
	2. Das Parkhaussystem sollte über eine Datenbankanbindung verfügen.
	
##Rahmenbedingungen##
	1.  zunächst keine

# User Stories #
	1. Als *Kunde* _benötige_ ich *einen Parkplatz*, um *mein Auto abstellen* zu können.
	2. Als *Kunde* _benötige_ ich *ein Kennzeichen*, um *identifiziert werden* zu können.
	3. Als *Kunde* _benötige_ ich *einen Parkschein*, um *in das Parkhaus einfahren* zu können.
	4. Als *Kunde* _möchte_ ich *einen freien Parkplatz zugewiesen bekommen*, um *mein Auto dort abstellen* zu können.
	5. Als *Kunde* _möchte_ ich *einen Parkplatz*, um *mein Auto abstellen* zu können.
	6. Als *Kunde* _möchte_ ich *meine Parkkosten sehen*, um *das Parkticket bezahlen* zu können.
	7. Als *Kunde* _möchte_ ich *meinen Parkschein bezahlen*, um *das Parkhaus verlassen* zu können.

	8. Als *Manager* _möchte_ ich *die monatlichen Kunden sehen*, um *einen Überblick behalten* zu können.
	9. Als *Manager* _möchte_ ich *die monatlichen Einnahmen sehen*, um *eine Buchhaltung erstellen* zu können.
# Kanban-Board #

# Gantt-Diagramm #
https://bitbucket.org/parkhausenterprisesystems/parkhaus/wiki/Zeitplan
# Priorisierung der Userstories #
| User Story | Aufwand | Mehrwert | Strafe | Risiko |
|------------|---------|----------|--------|--------|
| US 1       | 5       | 10       | 7      | 1      |
| US 2       | 6       | 10       | 8      | 1      |
| US 3       | 4       | 8        | 5      | 2      |
| US 4       | 6       | 8        | 5      | 2      |
| US 5       | 5       | 10       | 8      | 1      |
| US 6       | 5       | 9        | 10     | 1      |
| US 7       | 6       | 10       | 5      | 1      |
| US 8       | 8       | 10       | 7      | 1      |
| US 9       | 8       | 10       | 7      | 1      |
# UML Klassendiagramm #
https://ultimapp.de/parkhaus/static/Parkhaus.svg